// Import the electrum client and related information.
import { ElectrumClient, ElectrumNetworkOptions } from '@electrum-cash/network';

// ...
import type { ElectrumSocket } from '@electrum-cash/network';

// Import electrum and blockchain related type information.
import type { ElectrumProtocolEvents, VersionNumbers, ElectrumNotification, AddressSubscribeNotification, TransactionSubscribeNotification, TransactionDsProofNotification, HeadersSubscribeNotification } from './interfaces';

/**
 * Defines the required version of the electrum cash protocol
 * @group Utilities
 */
export const requiredProtocolVersion = '1.5.2';

/**
 * Extracts the major, minor and patch numbers from an electrum version string.
 * @ignore
 */
export async function extractVersionNumbers(versionString: string): Promise<VersionNumbers>
{
	// Define a regexp to extract the version numbers.
	const regexp = /(?<major>[0-9]+)(\.(?<minor>[0-9]+))?(\.(?<patch>[0-9]+))?/g;

	// Consume the iterator to get the single match.
	const { value: match } = versionString.matchAll(regexp).next();

	// Throw an error if we couldn't parse the version numbers.
	if(!match || !match.groups.major)
	{
		throw(new Error(`Could not extract version numbers from '${versionString}'.`));
	}

	// Return the extracted version numbers.
	return match.groups as unknown as VersionNumbers;
}

/**
 * Utility function that verifies that a provided electrum version string is sufficient for usage in this library.
 * @group Utilities
 */
export async function sufficientProtocolVersion(providedVersion: string): Promise<boolean>
{
	const providedVersionNumbers = await extractVersionNumbers(providedVersion);
	const requiredVersionNumbers = await extractVersionNumbers(requiredProtocolVersion);

	// Return true if the provided major version exceeds required major version.
	if(providedVersionNumbers.major > requiredVersionNumbers.major)
	{
		return true;
	}

	// Return false if the provided major version is insufficient.
	if(providedVersionNumbers.major < requiredVersionNumbers.major)
	{
		return false;
	}

	// The major version is now the same.

	// Return true if the provided major version is the same, and the minor version exceeds required minor version.
	if(providedVersionNumbers.minor > requiredVersionNumbers.minor)
	{
		return true;
	}

	// Return false if the provided major version is insufficient.
	if(providedVersionNumbers.minor < requiredVersionNumbers.minor)
	{
		return false;
	}

	// The major and minor versions are now the same.

	// Return true if the provided major and minor versions are the same, and the patch version is sufficient.
	if(providedVersionNumbers.patch >= requiredVersionNumbers.patch)
	{
		return true;
	}

	// Return false since the patch version was not sufficient.
	return false;
}

/**
 * Handler function for all electrum notifications.
 *
 * This function determines what type of notification has been provided and emits the
 * same notification it received, but with added type information.
 * @ignore
 */
export async function processElectrumNotifications(notification: ElectrumNotification): Promise<void>
{
	// If this is an address status update notification..
	if(notification.method === 'blockchain.address.subscribe')
	{
		// Re-emit the event with full typing.
		this.emit(notification.method, notification as AddressSubscribeNotification);
	}

	// If this is a transaction status update notification..
	if(notification.method === 'blockchain.transaction.subscribe')
	{
		// Re-emit the event with full typing.
		this.emit(notification.method, notification as TransactionSubscribeNotification);
	}

	// If this is a transaction double spend notification..
	if(notification.method === 'blockchain.transaction.dsproof.subscribe')
	{
		// Re-emit the event with full typing.
		this.emit(notification.method, notification as TransactionDsProofNotification);
	}

	// If this is a block header notification..
	if(notification.method === 'blockchain.headers.subscribe')
	{
		// Re-emit the event with full typing.
		this.emit(notification.method, notification as HeadersSubscribeNotification);
	}
}

/**
 * Creates and initializes an electrum client in order
 * to provide typed electrum notification events.
 *
 * @emits blockchain.address.subscribe
 * @emits blockchain.transaction.subscribe
 * @emits blockchain.transaction.dsproof.subscribe
 * @emits blockchain.headers.subscribe
 *
 * @group Setup
 *
 * @param application       - your application name, used to identify to the electrum host.
 * @param socketOrHostname  - pre-configured electrum socket or fully qualified domain name or IP number of the host
 * @param options           - optional settings that change the default behavior of the network connection.
 *
 * @returns an electrum client that provides fully typed electrum notifications.
 */
export async function initializeElectrumClient(application: string, socketOrHostname: ElectrumSocket | string, options?: ElectrumNetworkOptions): Promise<ElectrumClient<ElectrumProtocolEvents>>
{
	// Store the electrum client as the initialized network provider.
	const electrumClient = new ElectrumClient<ElectrumProtocolEvents>(application, requiredProtocolVersion, socketOrHostname, options);

	// Hook up the electrum notification parser to the client.
	electrumClient.on('notification', processElectrumNotifications.bind(electrumClient));

	// Connect the electrum client with the electrum server in the background.
	electrumClient.connect();

	// Return the initialized electrum client.
	return electrumClient;
}
