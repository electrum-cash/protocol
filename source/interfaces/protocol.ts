import type { ElectrumClientEvents } from '@electrum-cash/network';
import type { AddressSubscribeNotification, TransactionSubscribeNotification, TransactionDsProofNotification, HeadersSubscribeNotification } from './electrum';

// Add protocol specific events.
export interface ElectrumProtocolEvents extends ElectrumClientEvents
{
	/**
	 * ...
	 * @eventProperty
	 */
	'blockchain.address.subscribe': [ AddressSubscribeNotification ];

	/**
	 * ...
	 * @eventProperty
	 */
	'blockchain.transaction.subscribe': [ TransactionSubscribeNotification ];

	/**
	 * ...
	 * @eventProperty
	 */
	'blockchain.transaction.dsproof.subscribe': [ TransactionDsProofNotification ];

	/**
	 * ...
	 * @eventProperty
	 */
	'blockchain.headers.subscribe': [ HeadersSubscribeNotification ];
}
