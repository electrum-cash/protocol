/**
 * This file documents externally defined 3rd party interfaces,
 * so we need to disable some linting rules that cannot be enforced.
 *
 * Furthermore, the descriptions of theses interfaces are copied almost
 * verbatim from https://electrum-cash-protocol.readthedocs.io/en/latest/
 */

/* eslint-disable camelcase */

/**
 * Return a histogram of the fee rates paid by transactions in the memory pool, weighted by transaction size.
 *
 * @memberof Mempool
 */
export type GetFeeHistogramRequest =
{
	/**  Must be: 'mempool.get_fee_histogram'. */
	method: 'mempool.get_fee_histogram';
};

/**
 * The histogram is an array of [fee, vsize] pairs, where vsize is the cumulative virtual size of mempool transactions with a fee rate in the interval [fee n-1, fee n], and fee n-1 > fee n.
 *
 * Fee intervals may have variable size. The choice of appropriate intervals is currently not part of the protocol.
 *
 * @memberof Mempool
 */
export type GetFeeHistogramEntry = [];
export type GetFeeHistogramResponse = GetFeeHistogramEntry[];
