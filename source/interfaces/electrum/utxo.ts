/**
 * This file documents externally defined 3rd party interfaces,
 * so we need to disable some linting rules that cannot be enforced.
 *
 * Furthermore, the descriptions of theses interfaces are copied almost
 * verbatim from https://electrum-cash-protocol.readthedocs.io/en/latest/
 */

/* eslint-disable camelcase */

import type
{
	BlockHeight,
	TransactionHash,
	OutputIndex,
	Satoshis,
} from '../blockchain';

import type
{
	TokenData,
} from './protocol';

/**
 * Return information for an unspent transaction output.
 *
 * @memberof Blockchain.UTXO
 */
export type UtxoGetInfoRequest =
{
	/**  Must be: 'blockchain.utxo.get_info'. */
	method: 'blockchain.utxo.get_info';

	/** The UTXO’s transaction hash as a hexadecimal string. */
	tx_hash: TransactionHash;

	/** The UTXO’s transaction output number. */
	out_n: OutputIndex;
};

/**
 * blockchain.utxo.get_info
 *
 * @memberof Blockchain.UTXO
 */
export type UtxoGetInfoEntry =
{
	confirmed_height?: BlockHeight;
	scripthash: string;
	value: Satoshis;
	token_data?: TokenData;
};
export type UtxoGetInfoResponse = null | UtxoGetInfoEntry;
