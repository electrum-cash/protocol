/**
 * This file documents externally defined 3rd party interfaces,
 * so we need to disable some linting rules that cannot be enforced.
 *
 * Furthermore, the descriptions of theses interfaces are copied almost
 * verbatim from https://electrum-cash-protocol.readthedocs.io/en/latest/
 */

/* eslint-disable camelcase */

import type
{
	AddressGetBalanceResponse,
	AddressGetHistoryResponse,
	AddressGetFirstUseResponse,
	AddressGetMempoolResponse,
	AddressListUnspentResponse,
	AddressSubscribeResponse,
	AddressUnsubscribeResponse,
} from './address';

// TODO: Separate from address, as every request has a different signature (scripthash instead of address), and subscribe has a different notification.

/**
 * blockchain.scripthash.get_balance
 *
 * @memberof Blockchain.Scripthash
 */
export type ScripthashGetBalanceResponse = AddressGetBalanceResponse;

/**
 * blockchain.scripthash.get_history
 *
 * @memberof Blockchain.Scripthash
 */
export type ScripthashGetHistoryResponse = AddressGetHistoryResponse;

/**
 * blockchain.scripthash.get_first_use
 *
 * @memberof Blockchain.Scripthash
 */
export type ScripthashGetFirstUseResponse = AddressGetFirstUseResponse;

/**
 * blockchain.scripthash.get_mempool
 *
 * @memberof Blockchain.Scripthash
 */
export type ScripthashGetMempoolResponse = AddressGetMempoolResponse;

/**
 * blockchain.scripthash.listunspent
 *
 * @memberof Blockchain.Scripthash
 */
export type ScripthashListUnspentResponse = AddressListUnspentResponse;

/**
 * blockchain.scripthash.subscribe
 *
 * @memberof Blockchain.Scripthash
 */
export type ScripthashSubscribeResponse = AddressSubscribeResponse;

/**
 * blockchain.scripthash.unsubscribe
 *
 * @memberof Blockchain.Scripthash
 */
export type ScripthashUnsubscribeResponse = AddressUnsubscribeResponse;
